This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

Below you will find some information on how to perform common tasks.<br>
You can find the most recent version of this guide [here](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## To Start the Project

### `yarn install`

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](#running-tests) for more information.

### `yarn run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

## CSS Architecture Choice

For this project I opted to use CSS-in-JS. Most of my work so far was done using SASS, but on my last job, I worked on a project that the client demanded CSS-in-JS (using material-ui). I enjoyed the experience of working just in JavaScript.

I'm still learning the intricacies and testing several different libraries, but JSS (the one upon react-jss is built on) seemed to have better performance and as the core is low level, it is more framework agnostic, that is, I would be able to using my knowledge in projects that are not made in React.

I made a theme with most of my variables and mixins (theme.js) and consumed it in my components (palette, typography, etc.).

The drawbacks so far, in my opinion, is that I could not integrate CSS files with JS files to be able to use the same variables (for some components not created by me, the day-picker for example). I know that it is feasible, I just did not have the time to make further research.

Other feature that could be improved is adding some dynamic vendor-prefixing. The library offers some official plugins for that, but I also could not integrate them. I know it's simple, it's just a question of doing some research.
