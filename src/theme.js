export default {
  breakpoints: {
    xs: 0,
    sm: 600,
    md: 960,
    lg: 1280,
    xl: 1920,
  },
  palette: {
    primary: '#F98100',
    secondary: '#79BD1A',
    text: {
      white: '#fff',
      lightGray: '#B5B5B5',
      mediumGray: '#555555',
      black: '#000'
    }
  },
  colorPrimary: 'green',
  typography: {
    '@font-face': [
      {
        fontFamily: 'Heebo',
        fontWeight: 100,
        src: 'url(./assets/fonts/Heebo-Light.ttf)'
      },
      {
        fontFamily: 'Heebo',
        fontWeight: 200,
        src: 'url(./assets/fonts/Heebo-Regular.ttf)'
      },
      {
        fontFamily: 'Heebo',
        fontWeight: 300,
        src: 'url(./assets/fonts/Heebo-Medium.ttf)'
      },
      {
        fontFamily: 'Montserrat',
        fontWeight: 100,
        src: 'url(./assets/fonts/Montserrat-Light.otf)'
      },
      {
        fontFamily: 'Montserrat',
        fontWeight: 200,
        src: 'url(./assets/fonts/Montserrat-Regular.otf)'
      },
      {
        fontFamily: 'Montserrat',
        fontWeight: 300,
        src: 'url(./assets/fonts/Montserrat-Bold.otf)'
      },
      {
        fontFamily: 'Montserrat',
        fontWeight: 400,
        src: 'url(./assets/fonts/Montserrat-ExtraBold.otf)'
      }
    ],
    primaryFont: '"Heebo", "Helvetica", sans-serif',
    secondaryFont: '"Montserrat", "Helvetica", sans-serif',
    display1: {
      fontSize: 36,
      fontFamily: '"Montserrat", "Helvetica", sans-serif',
      fontWeight: 400,
      '@media (min-width: 960px)': {
        fontSize: 76
      }
    },
    display4: {
      fontSize: 16,
      fontFamily: '"Montserrat", "Helvetica", sans-serif',
      fontWeight: 200,
      letterSpacing: 2.5,
      margin: '8px 0 12px',
      '@media (min-width: 960px)': {
        fontSize: 22
      }
    },
    display5: {
      fontSize: 14,
      fontFamily: '"Heebo", "Helvetica", sans-serif',
      fontWeight: 200,
      letterSpacing: 1.3,
      margin: '8px 0 12px',
      '@media (min-width: 960px)': {
        fontSize: 20
      }
    },
    display6: {
      fontSize: 12,
      fontFamily: '"Heebo", "Helvetica", sans-serif',
      fontWeight: 500,
      letterSpacing: 1.8,
      margin: '8px 0 12px',
      '@media (min-width: 960px)': {
        fontSize: 16
      }
    },
    body1: {
      fontSize: 12,
      fontFamily: '"Heebo", "Helvetica", sans-serif',
      fontWeight: 200,
      margin: '8px 0 12px',
      '@media (min-width: 960px)': {
        fontSize: 15
      }
    },
    caption: {
      fontSize: 12,
      fontFamily: '"Heebo", "Helvetica", sans-serif',
      fontWeight: 200,
    },
    link: {
      textDecoration: 'none',
      fontFamily: '"Heebo", "Helvetica", sans-serif',
      fontWeight: 200,
      fontSize: 15,
    }
  }
}
