import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

class Button extends Component {
  constructor(props) {
    super(props);
    this.mounted = false;
    this.state = {
      clicked: false
    };
  }

  /* Hack to avoid calling setState after element has unmounted
    https://reactjs.org/blog/2015/12/16/ismounted-antipattern.html
  */
  componentWillMount() {
    this.mounted = true;
  }
  componentWillUnmount() {
    this.mounted = false;
  }

  /* Create ripple effect on Click */
  rippleEffect = event => {
    const el = this.refs.button.getBoundingClientRect();
    const posX = event.pageX - el.left - 20;
    const posY = event.pageY / event.target.offsetHeight - 20;

    if (this.mounted) {
      this.setState({
        clicked: !this.state.clicked,
        posX,
        posY
      });
    }

    setTimeout(() => {
      if (this.mounted) {
        this.setState({
          clicked: !this.state.clicked
        });
      }
    }, 600);
  };

  onButtonClick = event => {
    this.rippleEffect(event);
    this.props.onClick();
  };

  render() {
    const { classes, children, extraClass, color } = this.props;
    return (
      <button
        className={[classes.button, classes[color], extraClass].join(' ')}
        onClick={event => this.onButtonClick(event)}
        ref="button"
      >
        {children || 'Button'}
        {this.state.clicked && (
          <span
            className={classes.ink}
            style={{ top: this.state.posY, left: this.state.posX }}
          />
        )}
      </button>
    );
  }
}

const { object, string } = PropTypes

Button.propTypes = {
  classes: object,
  children: string,
  extraClass: string,
  color: string,
};

Button.defaultProps = {
  color: 'primary'
};

const styles = theme => ({
  button: {
    overflow: 'hidden',
    padding: '14px 46px',
    position: 'relative',
    borderRadius: 25,
    fontFamily: theme.typography.primaryFont,
    fontWeight: 500,
    fontSize: 15,
    transition: 'all 0.2s cubic-bezier(0.60,1,0.70,0) 0s',
    width: '100%',
    '&:hover': {
      cursor: 'pointer'
    }
  },
  ink: {
    position: 'absolute',
    display: 'block',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
    width: '50px',
    height: '50px',
    borderRadius: '50%',
    transform: 'scale(0)',
    animation: 'ripple 0.6s ease-in-out'
  },
  legalText: {
    ...theme.typography.caption,
    color: theme.palette.text.lightGray,
    textAlign: 'center',
    maxWidth: '80%',
    margin: '0 auto',
    lineHeight: 1.6
  },
  link: {
    margin: 12,
    '& img': {
      width: 35
    }
  },
  primary: {
    backgroundColor: theme.palette.text.white,
    color: theme.palette.primary,
    border: `2px solid ${theme.palette.primary}`,
    '&:hover': {
      backgroundColor: theme.palette.primary,
      color: theme.palette.text.white
    }
  },
  secondary: {
    backgroundColor: theme.palette.secondary,
    color: theme.palette.text.white,
    border: `2px solid ${theme.palette.secondary}`,
    width: '100%',
    '&:hover': {
      backgroundColor: theme.palette.text.white,
      color: theme.palette.secondary
    }
  },
  '@keyframes ripple': {
    to: {
      opacity: 0,
      transform: 'scale(2.5)'
    }
  },
  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    button: {
      width: 'initial'
    },
    footer: {
      height: 160,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-around'
    }
  }
});

export default injectSheet(styles)(Button);
