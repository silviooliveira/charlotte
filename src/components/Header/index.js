import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

import Brand from './Brand';
import NavBar from './NavBar';
import Background from '../../assets/hero.jpg';

const Header = ({ classes }) => {
  return (
    <header className={classes.background}>
      <NavBar />
      <Brand />
    </header>
  );
};

const { object } = PropTypes;

Header.propTypes = {
  classes: object
};

const styles = theme => ({
  background: {
    backgroundImage: `url(${Background})`,
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    width: '100%',
    height: 300
  },
  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    background: {
      height: 600
    }
  }
});

export default injectSheet(styles)(Header);
