import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

const NavBar = ({ classes }) => {
  return (
    <ul className={classes.menu}>
      <li className={classes.item}>
        <a href="https://www.redventures.com/">The Queen City</a>
      </li>
      <li className={classes.item}>
        <a href="https://www.redventures.com/">My Reservations</a>
      </li>
      <li className={classes.item}>
        <a href="https://www.redventures.com/">Guide</a>
      </li>
    </ul>
  );
};

const { object } = PropTypes;

NavBar.propTypes = {
  classes: object
};

const styles = theme => ({
  menu: {
    width: '100%',
    margin: 0,
    padding: 0,
    listStyleType: 'none'
  },
  item: {
    display: 'inline-block',
    '& a': {
      ...theme.typography.link,
      color: theme.palette.text.white,
      display: 'block',
      margin: 20,
      paddingBottom: 8,
      borderBottom: '1.6px solid transparent',
      '&:hover': {
        borderBottomColor: theme.palette.text.white
      }
    }
  },
  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    background: {
      height: 600
    }
  }
});

export default injectSheet(styles)(NavBar);
