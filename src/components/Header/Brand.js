import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

import Crown from '../../assets/crown.svg';

const Brand = ({ classes }) => {
  return (
    <div className={classes.container}>
      <img src={Crown} alt="Crown Logo" className={classes.image} />
      <span className={classes.subtitle}>welcome to</span>
      <span className={classes.title}>charlotte</span>
      <span className={classes.subtitle}>the queen city</span>
    </div>
  );
};

const { object } = PropTypes;

Brand.propTypes = {
  classes: object
};

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    maxWidth: '80%',
    margin: '0 auto'
  },
  title: {
    ...theme.typography.display1,
    textTransform: 'uppercase',
    color: theme.palette.text.white,
    letterSpacing: 10.6,
    margin: [10, 0],
    padding: [3, 5, 5, 5],
    borderTop: `2px solid ${theme.palette.text.white}`,
    borderBottom: `2px solid ${theme.palette.text.white}`
  },
  subtitle: {
    ...theme.typography.display4,
    textTransform: 'uppercase',
    color: theme.palette.text.white,
    letterSpacing: 2.6
  },
  image: {
    display: 'block',
    width: 40,
    margin: '0 auto',
    marginBottom: 8
  },
  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    title: {
      padding: [5, 15, 10, 15]
    },
    image: {
      width: 55,
      marginBottom: 16
    },
    container: {
      marginTop: 30
    }
  }
});

export default injectSheet(styles)(Brand);
