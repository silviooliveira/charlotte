import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

import Facebook from '../../assets/facebook.svg';
import Instagram from '../../assets/instagram.svg';
import Twitter from '../../assets/twitter.svg';

const Footer = ({ classes }) => {
  return (
    <footer className={classes.footer}>
      <div className={classes.socialIconContainer}>
        <a href="https://facebook.com" className={classes.link}>
          <img src={Facebook} alt="Facebook Icon" />
        </a>
        <a href="https://twitter.com" className={classes.link}>
          <img src={Twitter} alt="Twitter Icon" />
        </a>
        <a href="https://instagram.com" className={classes.link}>
          <img src={Instagram} alt="Instagram Icon" />
        </a>
      </div>
      <div className={classes.legalText}>
        © 2004-2017 Visit Charlotte. All Rights Reserved. 500 S. College Street,
        Suite 300, Charlotte, NC 28202.
      </div>
    </footer>
  );
};

const { object } = PropTypes;

Footer.propTypes = {
  classes: object
};

const styles = theme => ({
  footer: {
    padding: [20, 0, 25],
    boxShadow: '0px 0px 30px 0px rgba(0,0,0,0.32)'
  },
  socialIconContainer: {
    display: 'flex',
    justifyContent: 'center'
  },
  link: {
    margin: 12,
    '& img': {
      width: 35
    }
  },
  legalText: {
    ...theme.typography.caption,
    color: theme.palette.text.lightGray,
    textAlign: 'center',
    maxWidth: '80%',
    margin: '0 auto',
    lineHeight: 1.6
  },
  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    footer: {
      height: 160,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-around'
    }
  }
});

export default injectSheet(styles)(Footer);
