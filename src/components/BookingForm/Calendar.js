import React from 'react';
import PropTypes from 'prop-types';
import DayPicker, { DateUtils } from 'react-day-picker';
import 'react-day-picker/lib/style.css';
import './daypicker.css';

const Weekday = ({ weekday, className, localeUtils, locale }) => {
  const weekdayName = localeUtils.formatWeekdayLong(weekday, locale);
  return (
    <div className={className} title={weekdayName}>
      {weekdayName.slice(0, 1)}
    </div>
  );
};

const Calendar = props => {
  const handleDayClick = day => {
    const range = DateUtils.addDayToRange(day, props.range);
    props.onUpdateRange(range);
  };

  const { from, to } = props.range;
  const modifiers = { start: from, end: to };
  return (
    <DayPicker
      className="Selectable"
      weekdayElement={<Weekday />}
      numberOfMonths={1}
      selectedDays={[from, { from, to }]}
      modifiers={modifiers}
      onDayClick={handleDayClick}
    />
  );
};

const { object, func } = PropTypes;

Calendar.propTypes = {
  range: object,
  onUpdateRange: func
};

export default Calendar;
