import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import moment from 'moment';

const DateBox = ({ classes, range }) => {
  const getDate = date => {
    const month = moment(date).format('MMMM');
    const day = moment(date).format('DD');
    const year = moment(date).format('YYYY');
    return (
      <span>
        {month} <strong>{day}</strong>, {year}
      </span>
    );
  };

  return (
    <div className={classes.container}>
      <div className={classes.flexItem}>
        <h4 className={classes.title}>check-in</h4>
        <h5 className={classes.subtitle}>
          {range.from ? getDate(range.from) : <span> Choose a date </span>}
        </h5>
      </div>
      <div className={classes.flexItem}>
        <h4 className={classes.title}>check-out</h4>
        <h5 className={classes.subtitle}>
          {range.to ? getDate(range.to) : <span> Choose a date </span>}
        </h5>
      </div>
    </div>
  );
};

const { object } = PropTypes;

DateBox.propTypes = {
  range: object,
  classes: object
};

const styles = theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 40
  },
  flexItem: {
    flex: '1 1 50%',
    textAlign: 'center'
  },
  subtitle: {
    ...theme.typography.display5,
    color: theme.palette.text.lightGray
  },
  title: {
    ...theme.typography.display4,
    fontFamily: theme.typography.primaryFont,
    textTransform: 'uppercase',
    color: theme.palette.text.mediumGray
  },
  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    container: {
      flexDirection: 'column',
      marginBottom: 0
    },
    flexItem: {
      marginBottom: 30,
      textAlign: 'left'
    }
  }
});

export default injectSheet(styles)(DateBox);
