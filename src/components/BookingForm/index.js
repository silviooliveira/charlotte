import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import MediaQuery from 'react-responsive';

import DateBox from './DateBox';
import Calendar from './Calendar';
import Button from '../general/Button';

const BookingForm = props => {
  const { classes, range, error, onSearchClick } = props;

  const onUpdateRange = range => {
    props.onUpdateRange(range);
  };

  return (
    <section className={classes.bookingContainer}>
      <h4 className={classes.title}>Select the dates to stay in Charlotte</h4>
      <div className={classes.flexContainer}>
        <div className={classes.flexItem}>
          <DateBox range={range} />
          <MediaQuery minWidth={960}>
            <Button onClick={onSearchClick} color="primary">
              Search hotels
            </Button>
            {error && (
              <p className={classes.error}>
                You need to select a check-in and check-out date before you can
                search for a hotel
              </p>
            )}
          </MediaQuery>
        </div>
        <div className={classes.flexItem} style={{ textAlign: 'center' }}>
          <Calendar range={range} onUpdateRange={onUpdateRange} />
        </div>
      </div>
      <MediaQuery maxWidth={960}>
        <Button
          onClick={props.onSearchClick}
          extraClass={classes.button}
          color="primary"
        >
          Search hotels
        </Button>
        {error && (
          <p className={classes.error}>
            You need to select a check-in and check-out date before you can
            search for a hotel
          </p>
        )}
      </MediaQuery>
      <div className={classes.shade} />
    </section>
  );
};

const { object, func, bool } = PropTypes;

BookingForm.propTypes = {
  classes: object,
  onUpdateRange: func,
  range: object,
  onSearchClick: func,
  error: bool
};

BookingForm.defaultProps = {
  color: 'primary'
};

const styles = theme => ({
  bookingContainer: {
    borderRadius: 12,
    width: '100%',
    padding: 25,
    position: 'relative',
    backgroundColor: theme.palette.text.white,
    marginBottom: 60
  },
  button: {
    display: 'block',
    margin: '0 auto'
  },
  error: {
    ...theme.typography.caption,
    color: 'red',
    maxWidth: 250
  },
  flexContainer: {
    display: 'flex',
    flexDirection: 'column',
    marginBottom: 30
  },
  title: {
    ...theme.typography.display4,
    textAlign: 'center',
    letterSpacing: 2.5,
    marginBottom: 50
  },
  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    bookingContainer: {
      padding: [60, 80],
      maxWidth: 835,
      margin: [-145, 'auto', 60, 'auto']
    },
    footer: {
      height: 160,
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'space-around'
    },
    flexContainer: {
      flexDirection: 'row',
      marginBottom: 0
    },
    flexItem: {
      flex: '1 1 50%'
    },
    shade: {
      borderRadius: 12,
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 60,
      boxShadow: '0px 0px 100px 0px rgba(0,0,0,0.2)',
      zIndex: -1
    }
  }
});

export default injectSheet(styles)(BookingForm);
