import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from 'rc-checkbox';
import injectSheet from 'react-jss';
import './css/checkbox.css';

import StarFilled from '../../assets/star-filled.svg';
import StarOutline from '../../assets/star-outline.svg';

const StyledCheckbox = props => {
  const { classes, rate, name, ratingFilter } = props;

  const onChange = e => {
    let index = Number(name.substr(4)) - 1;
    props.onRatingFilterChange(index);
  };

  const getStarsRating = rate => {
    const stars = [];
    for (let i = 1; i < 6; i++) {
      let item;
      if (rate < i) {
        item = (
          <img className={classes.star} src={StarOutline} alt="star" key={i} />
        );
      } else {
        item = (
          <img className={classes.star} src={StarFilled} alt="star" key={i} />
        );
      }
      stars.push(item);
    }
    return stars;
  };

  return (
    <div className={classes.container}>
      <Checkbox
        name={name}
        defaultChecked={false}
        value={ratingFilter}
        type="checkbox"
        onChange={onChange}
      />
      {getStarsRating(rate)}
    </div>
  );
};

const { object, func, bool, string, number } = PropTypes;

StyledCheckbox.propTypes = {
  classes: object,
  rate: number,
  name: string,
  ratingFilter: bool,
  onRatingFilterChange: func
};

const styles = theme => ({
  star: {
    width: 27,
    marginRight: 5
  },
  container: {
    display: 'flex',
    alignItems: 'flex-end',
    marginBottom: 20
  }
});

export default injectSheet(styles)(StyledCheckbox);
