import React from 'react';
import PropTypes from 'prop-types';
import InputRange from 'react-input-range';
import injectSheet from 'react-jss';

import './css/rangeinput.css';

const RangeInput = props => {
  const {
    classes,
    valueFilter,
    onValueFilterChange,
    onValueFilterChangeComplete
  } = props;
  return (
    <div className={classes.inputContainer}>
      {/* eslint-disable */}
      <InputRange
        draggableTrack
        maxValue={700}
        minValue={100}
        value={valueFilter}
        onChange={value => {
          onValueFilterChange(value);
        }}
        onChangeComplete={onValueFilterChangeComplete}
      />
      <div className={classes.minMaxContainer}>
        <div className={classes.minMaxItem}>
          <span className={classes.subtitle}>Min</span>
          <span className={classes.value}>${valueFilter.min}</span>
        </div>
        <div className={classes.minMaxItem} style={{ textAlign: 'right' }}>
          <span className={classes.subtitle}>Max</span>
          <span className={classes.value}>${valueFilter.max}</span>
        </div>
      </div>
    </div>
  );
};

const { object, func } = PropTypes;

RangeInput.propTypes = {
  classes: object,
  valueFilter: object,
  onValueFilterChange: func,
  onValueFilterChangeComplete: func
};

const styles = theme => ({
  inputContainer: {
    width: '100%'
  },
  subtitle: {
    marginBottom: 5
  },
  minMaxContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    marginTop: 16,
    '& span': {
      display: 'block',
      fontFamily: theme.typography.primaryFont,
      color: theme.palette.text.lightGray,
      fontSize: 14
    }
  },
  value: {
    fontSize: '22px !important',
    color: 'rgba(249, 129, 0, 0.47) !important',
    letterSpacing: 2.5
  }
});

export default injectSheet(styles)(RangeInput);
