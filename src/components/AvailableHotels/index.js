import React from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import moment from 'moment';
import HotelCard from './HotelCard';

import StyledCheckbox from './StyledCheckbox';
import RangeInput from './RangeInput';

const getNights = range => {
  let from = moment(range.from);
  let to = moment(range.to);
  let nights = to.diff(from, 'days');
  return nights;
};

const AvailableHotels = props => {
  const {
    range,
    showHotels,
    hotels,
    classes,
    ratingFilter,
    onRatingFilterChange,
    valueFilter,
    onValueFilterChange,
    onValueFilterChangeComplete
  } = props;
  if (!hotels || !range.to || !range.from || !showHotels) {
    return null;
  }
  const nights = getNights(range);

  const to = moment(range.to).format('LL');
  const from = moment(range.from).format('LL');

  if (to === from) {
    return null;
  }

  const hotelCards = hotels.map(item => (
    <HotelCard key={item.name} hotel={item} nights={nights} />
  ));

  return (
    <div className={classes.body}>
      <h4 className={classes.title}>
        Best choices between {from} and {to}.
      </h4>
      <section className={classes.container}>
        <aside className={classes.aside}>
          <h4
            className={classes.title}
            style={{ textAlign: 'left', marginBottom: 30 }}
          >
            Filters
          </h4>
          <p className={classes.subtitle}>
            Price Range per <strong>night</strong>
          </p>
          <RangeInput
            valueFilter={valueFilter}
            onValueFilterChange={onValueFilterChange}
            onValueFilterChangeComplete={onValueFilterChangeComplete}
          />
          <hr className={classes.divider} />
          <p className={classes.starTitle}>Stars</p>
          <StyledCheckbox
            rate={1}
            name="rate1"
            ratingFilter={ratingFilter[0]}
            onRatingFilterChange={onRatingFilterChange}
          />
          <StyledCheckbox
            rate={2}
            name="rate2"
            ratingFilter={ratingFilter[1]}
            onRatingFilterChange={onRatingFilterChange}
          />
          <StyledCheckbox
            rate={3}
            name="rate3"
            ratingFilter={ratingFilter[2]}
            onRatingFilterChange={onRatingFilterChange}
          />
          <StyledCheckbox
            rate={4}
            name="rate4"
            ratingFilter={ratingFilter[3]}
            onRatingFilterChange={onRatingFilterChange}
          />
          <StyledCheckbox
            rate={5}
            name="rate5"
            ratingFilter={ratingFilter[4]}
            onRatingFilterChange={onRatingFilterChange}
          />
        </aside>
        <main className={classes.main}>
          {hotelCards.length ? (
            hotelCards
          ) : (
            <h4
              className={classes.title}
              style={{ textAlign: 'left', marginBottom: 30 }}
            >
              There's no hotel available in your range. Try to change the
              filter.
            </h4>
          )}
        </main>
      </section>
    </div>
  );
};

const { object, func, array } = PropTypes;

AvailableHotels.propTypes = {
  classes: object,
  range: object,
  hotels: array,
  ratingFilter: array,
  valueFilter: object,
  onValueFilterChange: func,
  onValueFilterChangeComplete: func
};

const styles = theme => ({
  body: {
    maxWidth: 1400,
    margin: '0 auto',
    padding: 20
  },
  title: {
    ...theme.typography.display4,
    textAlign: 'center',
    letterSpacing: 2.5,
    lineHeight: 1.6
  },
  divider: {
    backgroundColor: theme.palette.text.lightGray,
    height: '1px',
    width: '100%',
    border: 'none',
    marginTop: 30
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%'
  },
  starTitle: {
    fontFamily: theme.typography.primaryFont,
    fontSize: 12.8,
    color: theme.palette.text.lightGray,
    fontWeight: 200,
    margin: [20, 0]
  },
  subtitle: {
    ...theme.typography.body1,
    color: theme.palette.text.black
  },
  aside: {
    marginBottom: 40
  },
  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    container: {
      padding: [80, 20],
      flexDirection: 'row'
    },
    main: {
      flex: '2 1 80%'
    },
    aside: {
      flex: '1 1 18%',
      marginRight: 50
    }
  }
});

export default injectSheet(styles)(AvailableHotels);
