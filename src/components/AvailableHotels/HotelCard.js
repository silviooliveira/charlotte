import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';
import MediaQuery from 'react-responsive';

import Start from '../../assets/star-filled.svg';
import Button from '../general/Button';
import Chart from './Chart';

class HotelCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      history: false
    };
  }

  renderStarts = rate => {
    const stars = [];
    for (let i = 0; i < rate; i++) {
      let item = (
        <img
          src={Start}
          alt="rate star"
          className={this.props.classes.star}
          key={`star${i}`}
        />
      );
      stars.push(item);
    }
    return stars;
  };

  renderCurrency = number => {
    if (Number.isInteger(number)) {
      let integer = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD'
      }).format(number);
      return integer.replace('.00', '');
    } else {
      return new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
      }).format(number);
    }
  };

  toggleHistory = () => {
    this.setState(state => ({
      history: !state.history
    }));
  };

  render() {
    const { classes, hotel, nights } = this.props;
    return (
      <div className={classes.container}>
        {/* Display for mobile devices*/}
        <MediaQuery maxWidth={960}>
          <div className={classes.flexContainer}>
            {!this.state.history && (
              <div className={classes.imageContainer}>
                <img src={hotel.image} alt={hotel.name} />
              </div>
            )}
            <div className={classes.descriptionContainer}>
              {this.state.history ? (
                <Chart
                  toggleHistory={this.toggleHistory}
                  price_history={hotel.price_history}
                  height="100px"
                />
              ) : (
                <Fragment>
                  <div className={classes.infoContainer}>
                    <div>{this.renderStarts(hotel.rate)}</div>
                    <h4 className={classes.hotelName}>{hotel.name}</h4>
                    <p className={classes.description}>{hotel.description}</p>
                  </div>
                  <div className={classes.pricesContainer}>
                    <div>
                      <h5 className={classes.total}>
                        Total{' '}
                        <strong>{`${nights} ${
                          nights === 1 ? 'night' : 'nights'
                        }`}</strong>
                      </h5>
                      <h2 className={classes.priceBigger}>
                        {this.renderCurrency(hotel.price * nights)}
                      </h2>
                    </div>
                    <div>
                      <h6 className={classes.perNight}>Per night</h6>
                      <h6 className={classes.priceSmaller}>
                        {this.renderCurrency(hotel.price)}
                      </h6>
                    </div>
                  </div>
                </Fragment>
              )}
            </div>
          </div>
          <div>
            <Button
              onClick={() => console.log('clicked')}
              extraClass={classes.button}
              color="primary"
            >
              Book Now
            </Button>
            <Button onClick={this.toggleHistory} color="secondary">
              Price History
            </Button>
          </div>
        </MediaQuery>

        {/* Display for medium and large devices*/}
        <MediaQuery minWidth={960} component="div">
          <div className={classes.flexContainer}>
            <div className={classes.imageContainer}>
              <img src={hotel.image} alt={hotel.name} />
            </div>
            <div className={classes.descriptionContainer}>
              {this.state.history ? (
                <Chart
                  toggleHistory={this.toggleHistory}
                  price_history={hotel.price_history}
                />
              ) : (
                <Fragment>
                  <div className={classes.infoContainer}>
                    <div>{this.renderStarts(hotel.rate)}</div>
                    <h4 className={classes.hotelName}>{hotel.name}</h4>
                    <p className={classes.description}>{hotel.description}</p>
                    <div>
                      <Button
                        onClick={() => console.log('clicked')}
                        extraClass={classes.button}
                        color="primary"
                      >
                        Book Now
                      </Button>
                      <Button
                        onClick={this.toggleHistory}
                        extraClass={classes.button}
                        color="secondary"
                      >
                        Price History
                      </Button>
                    </div>
                  </div>
                  <div className={classes.pricesContainer}>
                    <div>
                      <h5 className={classes.total}>
                        Total{' '}
                        <strong>{`${nights} ${
                          nights === 1 ? 'night' : 'nights'
                        }`}</strong>
                      </h5>
                      <h2 className={classes.priceBigger}>
                        {this.renderCurrency(hotel.price * nights)}
                      </h2>
                    </div>
                    <div>
                      <h6 className={classes.perNight}>Per night</h6>
                      <h6 className={classes.priceSmaller}>
                        {this.renderCurrency(hotel.price)}
                      </h6>
                    </div>
                  </div>
                </Fragment>
              )}
            </div>
          </div>
        </MediaQuery>
      </div>
    );
  }
}

const { object, number } = PropTypes;

HotelCard.propTypes = {
  classes: object,
  hotel: object,
  nights: number
};

const styles = theme => ({
  container: {
    overflow: 'hidden',
    borderRadius: 8,
    boxShadow: '0px 0px 14px 0px rgba(0,0,0,0.44)',
    marginBottom: 20,
    padding: 10
  },
  flexContainer: {
    display: 'flex',
    flexDirection: 'row',
    marginBottom: 15
  },
  imageContainer: {
    height: 'auto',
    overflow: 'hidden',
    flex: '1 1 45%',
    marginRight: 12,
    alignSelf: 'stretch',
    '& img': {
      height: '100%',
      objectFit: 'cover',
      objectPosition: 'center',
      display: 'block',
      width: '100%',
      borderRadius: 4
    }
  },
  descriptionContainer: {
    flex: '1 1 55%'
  },
  pricesContainer: {
    textAlign: 'right'
  },
  star: {
    width: 12
  },
  hotelName: {
    ...theme.typography.display6,
    color: theme.palette.primary,
    textTransform: 'uppercase'
  },
  description: {
    ...theme.typography.body1,
    color: theme.palette.text.lightGray,
    lineHeight: 1.4
  },
  total: {
    fontFamily: theme.typography.primaryFont,
    fontSize: 16,
    letterSpacing: 1.82,
    fontWeight: 100,
    margin: 0,
    '& strong': {
      fontWeight: 600
    }
  },
  priceBigger: {
    fontFamily: theme.typography.secondaryFont,
    fontSize: 30,
    letterSpacing: 3.41,
    fontWeight: 700,
    margin: 0,
    marginBottom: 10
  },
  perNight: {
    fontFamily: theme.typography.primaryFont,
    fontSize: 14,
    letterSpacing: 1.59,
    fontWeight: 100,
    margin: 0
  },
  priceSmaller: {
    fontFamily: theme.typography.secondaryFont,
    fontSize: 20,
    letterSpacing: 2.27,
    fontWeight: 700,
    margin: 0
  },
  button: {
    marginBottom: 10
  },

  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    container: {
      padding: 45,
      marginLeft: 60,
      marginBottom: 60,
      overflow: 'visible'
    },
    descriptionContainer: {
      display: 'flex'
    },
    imageContainer: {
      flex: 1,
      borderRadius: 8,
      boxShadow: '0px 0px 14px 0px rgba(0,0,0,0.44)',
      marginLeft: -100
    },
    infoContainer: {
      margin: [0, 20],
      display: 'flex',
      flexDirection: 'column'
    },
    description: {
      flexGrow: 2
    },
    pricesContainer: {
      position: 'relative',
      marginLeft: 25,
      minWidth: 140,
      display: 'flex',
      flexDirection: 'column',
      justifyContent: 'space-around',
      padding: [40, 0],
      '&::before': {
        content: '""',
        position: 'absolute',
        width: 1.5,
        backgroundColor: theme.palette.text.lightGray,
        top: 0,
        bottom: 0,
        left: -15
      }
    },
    priceBigger: {
      fontSize: 25
    },
    flexContainer: {
      marginBottom: 0
    },
    button: {
      marginRight: 20,
      marginBottom: 0
    }
  },
  [`@media (max-width: 1110px) and (min-width: ${theme.breakpoints.md}px)`]: {
    button: {
      width: '100%',
      marginTop: 10
    },
    infoContainer: {
      margin: 0
    }
  }
});

export default injectSheet(styles)(HotelCard);
