import React, { Component } from 'react';
import PropTypes from 'prop-types';
import injectSheet from 'react-jss';

import Arrow from '../../assets/back-description.svg';

class Chart extends Component {
  state = {
    activeIndex: undefined
  };

  handleActiveIndex = num => {
    if (num >= 0) {
      this.setState({
        activeIndex: num
      });
    } else {
      this.setState({
        activeIndex: undefined
      });
    }
  };

  renderBars = arr => {
    const maxValue = Math.max(...arr.map(o => o.value));
    return arr.map((item, i) => {
      let height = Math.round(item.value * 100 / maxValue);
      return (
        <div
          className={this.props.classes.bar}
          key={`bar${i}`}
          style={{
            height: `${height}%`,
            opacity: this.state.activeIndex === i ? 1 : 0.8
          }}
          onMouseEnter={() => this.handleActiveIndex(i)}
          onMouseLeave={() => this.handleActiveIndex(-1)}
        >
          {this.state.activeIndex === i && (
            <span className={this.props.classes.tooltip}>
              ${Math.round(item.value)}
            </span>
          )}
          <span className={this.props.classes.subtitle}>{item.month}</span>
        </div>
      );
    });
  };

  render() {
    const { classes, toggleHistory, price_history, height } = this.props;

    return (
      <div className={classes.chartContainer}>
        <div className={classes.header}>
          <h4 className={classes.hotelName}>price history for 2017</h4>
          <div className={classes.back} onClick={toggleHistory}>
            <img src={Arrow} className={classes.arrow} alt="Back arrow" />
            <div>Back to description</div>
          </div>
        </div>
        <div className={classes.flex} style={{ height: height }}>
          {this.renderBars(price_history)}
        </div>
      </div>
    );
  }
}

const { object, func, string, array } = PropTypes;

Chart.propTypes = {
  classes: object,
  toggleHistory: func,
  price_history: array,
  height: string
};

Chart.defaultProps = {
  height: '100%'
};

const styles = theme => ({
  flex: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginTop: 40,
    borderBottom: `2px solid ${theme.palette.text.lightGray}`
  },
  chartContainer: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    padding: '20px 0'
  },
  header: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  hotelName: {
    ...theme.typography.display6,
    color: theme.palette.primary,
    textTransform: 'uppercase'
  },
  arrow: {
    width: 24,
    marginRight: 10
  },
  back: {
    fontFamily: theme.typography.primaryFont,
    fontSize: 12,
    letterSpacing: 1.36,
    color: theme.palette.text.mediumGray,
    fontWeight: 100,
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer'
  },
  bar: {
    width: '8%',
    position: 'relative',
    background: 'linear-gradient(#F98100, #EE6F48)',
    borderRadius: '3px 3px 0 0',
    cursor: 'pointer'
  },
  subtitle: {
    fontFamily: theme.typography.primaryFont,
    fontSize: 12.8,
    letterSpacing: 1.45,
    color: theme.palette.text.lightGray,
    fontWeight: 100,
    position: 'absolute',
    left: '50%',
    transform: 'translateX(-50%)',
    bottom: -28
  },
  tooltip: {
    position: 'absolute',
    width: 60,
    height: 26,
    backgroundColor: '#484848',
    color: theme.palette.text.white,
    fontFamily: theme.typography.primaryFont,
    fontWeight: 100,
    letterSpacing: 1.7,
    borderRadius: 3,
    top: -36,
    left: '50%',
    transform: 'translateX(-50%)',
    fontSize: 15,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '&::after': {
      content: '""',
      position: 'absolute',
      width: 0,
      height: 0,
      borderWidth: 6,
      borderStyle: 'solid',
      borderColor: 'transparent',
      borderTopColor: '#484848',
      top: 24,
      left: 24
    }
  },
  [`@media (min-width: ${theme.breakpoints.md}px)`]: {
    bar: {
      width: '5%'
    },
    chartContainer: {
      padding: '0 0 20px 20px'
    }
  }
});

export default injectSheet(styles)(Chart);
