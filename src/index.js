import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { ThemeProvider } from 'react-jss';
import registerServiceWorker from './registerServiceWorker';
import theme from './theme';
import 'normalize.css/normalize.css';
import './index.css';

// import { create as createJss } from 'jss';
// import vendorPrefixer from 'jss-vendor-prefixer';
// import { JssProvider } from 'react-jss';
//
// const jssPrefix = createJss();
// jssPrefix.use(vendorPrefixer());

const StyledApp = () => (
  // <JssProvider jss={jssPrefix}>
    <ThemeProvider theme={theme}>
      <App />
    </ThemeProvider>
  // </JssProvider>
);


ReactDOM.render(<StyledApp />, document.getElementById('root'));
registerServiceWorker();
