import React, { Component } from 'react';

import Header from './components/Header'
import Footer from './components/Footer'
import BookingForm from './components/BookingForm'
import AvailableHotels from './components/AvailableHotels'

import myData from './assets/hotels.json';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showHotels: false,
      hotels: undefined,
      filteredHotels: undefined,
      range: {
        to: undefined,
        from: undefined
      },
      ratingFilter: [true, true, true, true, true],
      valueFilter: { min: 100, max: 700 },
      submitError: false
    }
  }

  filterHotels = () => {
    const { hotels, ratingFilter, valueFilter } = this.state;
    const filteredHotels = hotels.filter(hotel => {
      return ratingFilter[hotel.rate - 1] === true
    }).filter(hotel => {
      return hotel.price >= valueFilter.min && hotel.price <= valueFilter.max
    })
    this.setState({
      filteredHotels
    })
  }

  componentDidMount() {
    this.setState({
      hotels: myData.hotels
    }, () => this.filterHotels())
  }

  onUpdateRange = (range) => {
    this.setState({range}, () => {
      if(this.state.range.from && this.state.range.to) {
        this.setState({
          submitError: false
        })
      }
    })
  }

  onRatingFilterChange = (index) => {
    this.setState((state) => {
      let newRatingFilter = state.ratingFilter;
      newRatingFilter[index] = !state.ratingFilter[index]
      return { ratingFilter: newRatingFilter }
    }, () => this.filterHotels())
  }

  onValueFilterChange = (value) => {
    this.setState({
      valueFilter: value
    })
  }

  onValueFilterChangeComplete = () => {
    this.filterHotels()
  }

  onSearchClick = () => {
    const { range } = this.state;

    if( !range.from || !range.to ) {
        this.setState({ submitError: true });
        return;
    }

    this.setState({
      showHotels: true
    })

  }

  render() {
    const { filteredHotels, range, ratingFilter, valueFilter, showHotels, submitError } = this.state;
    return (
      <div>
        <Header />
        <BookingForm
          onUpdateRange={this.onUpdateRange}
          range={range}
          onSearchClick={this.onSearchClick}
          error={submitError}
        />
        <AvailableHotels
          showHotels={showHotels}
          range={range}
          hotels={filteredHotels}
          ratingFilter={ratingFilter}
          onRatingFilterChange={this.onRatingFilterChange}
          valueFilter={valueFilter}
          onValueFilterChange={this.onValueFilterChange}
          onValueFilterChangeComplete={this.onValueFilterChangeComplete}
        />
        <Footer />
      </div>
    );
  }
}

export default App;
